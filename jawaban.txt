1.  MEMBUAT DATABASE
    CREATE DATABASE myshop;

2. MEMBUAT TABEL
   use myshop

   create TABLE user(
        id integer primary key AUTO_INCREMENT,
        nama varchar (255),
        email varchar(255),
        password varchar(255)
    );
    CREATE TABLE categories (
        id integer primary key AUTO_INCREMENT,
        nama varchar (255),
    );

    create TABLE items(
        id integer primary key AUTO_INCREMENT,
        nama varchar(255),
        description varchar(255),
        price integer(15),
    	stock integer(20),
    	category_id integer,
    	foreign key (category_id) REFERENCES categories(id)
    );

3. MEMASUKAN DATA PADA TABEL

    tabel users
        INSERT INTO users(nama,email,password) values("Jhon Dhoe","Jhon@Doe.com","Jhon123"),("Jhon Dhoe","Jhon@Doe.com","Jenita123");

    tabel categories
        INSERT into categories(name) VALUES("gadged"),("cloth"),("men"),("women"),("branded");

    tabel items
        INSERT INTO items(nama,description,price,stock,category_id)VALUES("Samsungj5","HP keren Samsung",4000000,100,1),("Unikloh","Baju keren",500000,50,2),("IMHO Watch","Jam Tangan keren",2000000,10,1);


4. MENGAMBIL DATA

    A. Mengambil data items
        SELECT id,nama,email FROM `users`;
    B. Mengambil data item price > i Jt
        SELECT * FROM `items` WHERE price>1000000;
    C. SELECT items.nama,items.description,items.price,items.stock,items.category_id,categories.name FROM items INNER JOIN categories ON items.category_id=categories.id;    


5. UBAH ISI TABEL
    UPDATE items set price=2500000 WHERE id=1;
